FROM python:2.7
ADD . /cod 
WORKDIR /cod/
RUN pip install -r requirements.txt

EXPOSE 8082
ENTRYPOINT ["python"]
CMD ["app.py"]
